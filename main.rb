require 'sinatra'
require 'logger'

logger = Logger.new(STDOUT)
logger.level = Logger::WARN
logger.debug("Running forwarder for #{ENV['http_endpoint']}")

def validURL (logger, url)
	if url.to_s.start_with?"http://" or url.to_s.start_with?"https://"
		logger.debug("Valid: #{url}")
		return "#{url}"
	else
		logger.error("Invalid URL: #{url}, needs scheme http:// or https://")
		exit!
	end
end

validURL(logger, ENV['http_endpoint'])		

get '/' do
	logger.debug("Request: #{request.to_s}")
	redirect "#{ENV['http_endpoint']}"
end
