FROM ruby:2.4.3

ENV http_endpoint ""

COPY Gemfile /app/Gemfile
COPY main.rb /app/main.rb

WORKDIR /app
RUN bundle install

ENTRYPOINT ruby main.rb -o 0.0.0.0
