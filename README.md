Forwarder
===

This is a containerized application to redirect to the value of `http_endpoint` environment variable to forward to another URL. Use-case is very specific, and this is probably not what you were looking for.

Setup
---
If you do not plan to run this application as a Dockerized application, install the dependencies, and start the server:

```
cd forwarder ; \
bundle install && \
http_endpoint="your-site.com" ruby main.rb -o 0.0.0.0
```
after setting the `http_endpoint` target. 


Running the Docker Container
---

This application is available as a Docker image at `jmarhee/forwarder`. 

Modify forwarder.yaml to set that `endpoint` variable with the target URL, to forward requests from Kubernetes. Using with an Ingress, one can create a redirect from a FQDN to the target URL, for example.

On a flat Docker setup, you can create a forwarder using:

```
docker run -d -p 4569:4567 -e http_endpoint="your-site.com" jmarhee/forwarder
```

and then running behind Nginx or whatever proxy option you prefer.
